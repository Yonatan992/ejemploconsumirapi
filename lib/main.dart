import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

void main() {
  runApp(new MaterialApp(
    home: new HomePage(),
  ));
}

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => new HomePageState();
}
class HomePageState extends State<HomePage> {

  Map<String,dynamic> data;

  Future<String> getData() async {
    var response = await http.get(
        Uri.encodeFull("http://192.168.0.136/museoQR/public/api/countries"),
        headers: {
           "Accept": "application/json",
        }
    );

    this.setState(() {
      data = json.decode(response.body);
    });
    //print(data["data"][1]["nombre"]);   // where i inserted my own value "name"

    return "Success!";
    }

  @override
  void initState() {
    super.initState();
    this.getData();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Listado de paises"),
      ),
      body: new ListView.builder(
        itemCount: data == null ? 0 : data["data"].length,
        itemBuilder: (BuildContext context, int index) {
          return new Card(
            child: new Text(data["data"][index]["nombre"]),  // inserted my own value "name"
          );
        },
      ),
    );
  }
}